package home.family_helper.monitoring.monitoring

import com.rabbitmq.client.*
import org.springframework.web.bind.annotation.RestController

private val QUEUE_NAME = "family_helper_queue"

@RestController
class EventsConsumer {

    private lateinit var connection: Connection
    private lateinit var channel: Channel

    val events: MutableList<String> = mutableListOf()

    init {
        val factory = ConnectionFactory()
        factory.host = "localhost"
        factory.port = 5672
        connection = factory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
        println(" [*] Waiting for messages. To exit press CTRL+C")
    }

    fun receive() {
        channel.basicConsume(QUEUE_NAME, true, object : DefaultConsumer(channel) {
            override fun handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: ByteArray) {
                val event = String(body, charset("UTF-8"))
                println("received event: $event")
                events.add(event)
            }
        })
    }

}

class EventsProducer1 {

    private var connection: Connection
    private var channel: Channel

    init {
        val factory = ConnectionFactory()
        factory.host = "localhost"
        factory.port = 5672
        connection = factory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
    }

    fun publish() {
        val message = "Hi from producer1"
        channel.basicPublish("", QUEUE_NAME, null, message.toByteArray())
        println(" [x] Sent '" + message + "'")
    }
}

class EventsProducer2 {

    private var connection: Connection
    private var channel: Channel

    init {
        val factory = ConnectionFactory()
        factory.host = "localhost"
        factory.port = 5672
        connection = factory.newConnection()
        channel = connection.createChannel()
        channel.queueDeclare(QUEUE_NAME, false, false, false, null)
    }

    fun publish() {
        val message = "Hi from producer2"
        channel.basicPublish("", QUEUE_NAME, null, message.toByteArray())
        println(" [x] Sent '" + message + "'")
    }
}