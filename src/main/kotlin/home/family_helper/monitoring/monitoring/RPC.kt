package home.family_helper.monitoring.monitoring

import com.rabbitmq.client.*
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.atomic.AtomicLong

data class Greeting(val id: Long, val content: String)

@RestController
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(name = "name", defaultValue = "World") name: String) =
            Greeting(counter.incrementAndGet(), "Hello, $name")
}

private val RPC_QUEUE_NAME = "rpc_queue"

@RestController
class RPCClient {

    private lateinit var connection: Connection
    private lateinit var channel: Channel
    private lateinit var replyQueueName: String
    private lateinit var corrId: String
    init {
        val factory = ConnectionFactory()
        factory.host = "localhost"
        factory.port = 5672
        connection = factory.newConnection()
        channel = connection.createChannel()
        replyQueueName = channel.queueDeclare().queue
        corrId = UUID.randomUUID().toString()
    }

    fun call(message: String): String {

        val props = AMQP.BasicProperties.Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build()

        channel.basicPublish("", RPC_QUEUE_NAME, props, message.toByteArray(charset("UTF-8")))

        val response = ArrayBlockingQueue<String>(1)

        channel.basicConsume(replyQueueName, true, object : DefaultConsumer(channel) {
            override fun handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: ByteArray) {
                if (properties.correlationId == corrId) {
                    response.offer(String(body, charset("UTF-8")))
                }
            }
        })

        return response.take()
    }

    @GetMapping("/rpc")
    fun sendMessage(@RequestParam(name = "message", defaultValue = "Hi") message: String) = call(message)

}

class RPCServer {

    private lateinit var channel: Channel

    init {
        val factory = ConnectionFactory()
        factory.host = "localhost"
        factory.port = 5672
        val connection = factory.newConnection()
        channel = connection.createChannel()

        channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null)

        channel.basicQos(1)


    }

    fun fib(n: Int): Int {
        if (n == 0) return 0
        return if (n == 1) 1 else fib(n - 1) + fib(n - 2)
    }

    fun serve() {
        System.out.println(" [x] Awaiting RPC requests")

        val consumer = object : DefaultConsumer(channel) {
            override fun handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: ByteArray) {
                val replyProps = AMQP.BasicProperties.Builder()
                        .correlationId(properties.correlationId)
                        .build()
                val message = String(body, charset("UTF-8"))
                val n = Integer.parseInt(message)
                println(" [.] fib($message)")
                val response = fib(n).toString()
                channel.basicPublish("", properties.replyTo, replyProps, response.toByteArray())
                channel.basicAck(envelope.deliveryTag, false)
            }
        }

        channel.basicConsume(RPC_QUEUE_NAME, false, consumer)
        // Wait and be prepared to consume the message from RPC client.
        while (true) {
            synchronized(consumer) {
                (consumer as Object).wait()
            }
        }
    }
}
