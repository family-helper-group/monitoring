package home.family_helper.monitoring.monitoring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import kotlinx.coroutines.experimental.*

@SpringBootApplication
class MonitoringApplication

fun main(args: Array<String>) {
/*
   launch {
      RPCServer().serve()
   }
*/

   launch {
      EventsProducer1().publish()
   }

   launch {
      EventsProducer2().publish()
   }

   launch {
      EventsConsumer().receive()
   }

   Thread.sleep(300000)

//   runApplication<MonitoringApplication>(*args)
}
