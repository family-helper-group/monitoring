#This is monitoring and tracking service which monitors the family helper app critical events.

 RabbitMQ is installed on the local machine with the following command:

$ docker run -d --net=host --hostname monitoring-rabbit --name monitoring-rabbit rabbitmq:3

As you see it uses the network of the host node. Hence, proper configuration must be done for Kubernetes cluster.
